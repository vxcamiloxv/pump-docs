:orphan:

Setup on Debian "jessie" stable
-------------------------------

We're going to install pump.io from the npm registry, use MongoDB for
the database and proxy pump.io behind the nginx web server.

Install basic requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~

Install the requirements via the Debian package manager.

::

    $ sudo apt-get install mongodb nginx graphicsmagick git

Debian jessie provides nodejs of version 0.10, while pump.io after
version 2.0.0 beta 1 requires nodejs 4.x. Therefore, it must be
updated from upstream.

::

    $ curl -sL https://deb.nodesource.com/setup_4.x | sudo bash -
    $ sudo apt-get install -y nodejs

You may want to also consider running ``sudo rm -r
/usr/local/lib/node_modules`` to remove all currently-installed npm
packages. Blowing away everything already installed is optional but
strongly recommended as npm v1 (which is shipped by Debian) is prone
to creating broken, half-installed directory structures that interfere
with future installations. In addition, Debian's npm has a default
install path of ``/usr/local/lib/node_modules``, whereas upstream
installs to ``/usr/lib/node_modules``. Simply removing the former path
makes everything much less confusing.

If you want to keep your currently-installed packages the best way to
do so is to save the list of installed packages, proceed with the
removal, then reinstall using upstream npm. Conveniently, you can get
a list of installed packages by doing ``npm ls -g --depth=0
--prefix=/usr/local``.

Install and set up pump.io
~~~~~~~~~~~~~~~~~~~~~~~~~~

Next, we create a ``pumpio`` user and group and create the needed folders:

::

    $ sudo groupadd pumpio
    $ sudo useradd -d /var/lib/pumpio -m -r -g pumpio pumpio

    $ sudo mkdir -p /var/lib/pumpio/uploads
    $ sudo chown -R pumpio:pumpio /var/lib/pumpio/uploads

    $ sudo mkdir /var/log/pumpio/
    $ sudo chown pumpio:pumpio /var/log/pumpio/

Next, install pump.io itself, along with the MongoDB Databank driver:

::

    $ sudo npm install -g pump.io databank-mongodb@0.19.2

Finally pump.io needs a configuration file in
``/etc/pump.io.json`` with the following content (adapted to
your situation):

::

    {
        "driver":  "mongodb",
        "params": {"host":"localhost","dbname":"pumpio"},
        "hostname":  "your.pump.com",
        "address": "127.0.0.1",
        "port": 8080,
        "urlPort": 443,
        "secret":  "somerandomstringhere",
        "key":  "/path/to/your/ssl-cert.crt",
        "cert":  "/path/to/your/ssl-cert.key",
        "noweb":  false,
        "site":  "your.pump.com",
        "owner":  "Your Name",
        "ownerURL":  "http://your.site.com/",
        "nologger": false,
        "logfile": "/var/log/pumpio/pumpio.log",
        "serverUser":  "pumpio",
        "datadir": "/var/lib/pumpio",
        "enableUploads": true,
        "debugClient": false,
        "firehose": "ofirehose.com",
        "disableRegistration": true,
        "noCDN": true,
        "requireEmail": false,
        "compress": true,
        "smtpserver": "localhost",
        "proxyWhitelist": ["avatar3.status.net", "avatar.identi.ca", "secure.gravatar.com"]
    }

In particular you need to replace ``your.pump.com`` which your actual
domain name.

.. WARNING:: If you're connecting your pump.io site with other
             software (such as federated servers or using Web
             clients), please note that most of them save OAuth keys
             based on your hostname and listening port. The following
             changes may make your relationships stop working.

             * Change of hostname
             * Change of port (from 8000 to 80 or even from HTTP to
               HTTPS)
             * Clearing your database or clearing some tables
             * Changing user nicknames

             We realize that these kind of changes are normal when
             someone's experimenting with new software, and there are
             (early, tentative) plans to make the software more robust
             in the face of this kind of change without sacrificing
             security, but for now it's a good idea to decide on your
             "real" domain name first before making connections to
             other sites.

Make sure that the pumpio user can access your SSL certs.

You can now try and see if it works:

::

    $ sudo -u pumpio pump

This starts up pump.io running on port 8080, this will be proxied
behind nginx which will serve it on port 443 (https).

Setup nginx proxying
~~~~~~~~~~~~~~~~~~~~

Create the file ``/etc/nginx/sites-available/pump`` with the following
content (adapted to your situation):

::

    upstream pumpiobackend {
      server 127.0.0.1:8080 max_fails=3;
    }

    server {
      server_name your.pump.com;
      rewrite ^ https://your.pump.com$request_uri?;
    }

    server {
      listen 443 ssl;
      server_name your.pump.com;

      ssl_certificate  /path/to/your/ssl-cert.crt;
      ssl_certificate_key  /path/to/your/ssl-cert.key;

      access_log /var/log/nginx/pumpio.access.log;
      error_log /var/log/nginx/pumpio.error.log;

      client_max_body_size 500m;

      keepalive_timeout 75 75;
      gzip_vary off;

      location / {
        proxy_http_version 1.1;
        proxy_set_header Host $http_host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header X-Real-IP $remote_addr;
    
        proxy_redirect off;
    
        proxy_buffers 16 32k;
        proxy_cache off;
        proxy_connect_timeout 60s;
        proxy_read_timeout 60s;
        proxy_pass https://pumpiobackend;

        proxy_pass_header Server;
      }
    }

You will of course have to provide your own SSL cert and put them in
the right path. Make sure that the certificate key is not world
readable! Currently the easiest (and cheapest) way to get such a
certificate is by using `Certbot and Let's Encrypt
<configuration/certbot.html>`_.

Finally you can link the file you created to enable it (in a root
shell):

::

    # cd /etc/nginx/sites-enabled
    # ln -s ../sites-available/pump .

If everything is set up correctly you should now be able to restart
nginx and access the site in your web browser:

::

    $ sudo systemctl restart nginx

Before you start using your site make sure have settled on the correct
hostname. You can't switch that around later without breaking
federation badly.

Start pump.io automatically using systemd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You probably want to make sure pump.io is started at boot and
monitored thereafter. You can accomplish this with systemd, and
pump.io ships a systemd file specifically for this purpose.

To use said systemd file, see `using the upstream pump.io systemd unit
<administration/upstream-systemd-unit.html>`_.

If everything seems to be working, you're done! Congratulations!

Upgrading pump.io
~~~~~~~~~~~~~~~~~

If you later wish to upgrade your pump.io server software, e.g. to the
latest version on npm (or even to git master), you can do something
like the following. (Note: you might want to backup your database
first using ``mongodump``, see the migration notes below.)

First, stop pump.io and switch to the pumpio user.

::

   $ sudo systemctl stop pump.io@mongodb

Next, install the latest version with npm:
   
::

   $ sudo npm install -g pump.io

It's a good idea to update the database stuff at the same time:

::
   
   $ sudo npm install -g databank-mongodb@0.19.2

If you're unsure, especially after a big upgrade, you can always test
it first directly by running ``sudo -u pumpio pump`` as above. If all
seems well you can Ctrl-C and start it for real.

::
   
   $ sudo systemctl start pump.io@mongodb
