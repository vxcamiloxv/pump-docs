Using a web server proxy
========================

pump.io is designed to be a standalone server. You do not need to set
up an Apache or nginx or lighttpd Web server in front of it. In fact,
that's going to make things harder for you:

* Stuff like WebSockets is going to work less well
* It will makes things harder to debug
* Future planned pump.io features, like automatic, out-of-the-box
  HTTPS management may not work as intended

The pump.io project strongly recommends against doing this if at all
possible.

If you really insist, check the configuration options carefully. If
you want http://pump.yourdomain.example/ to proxy to the pump.io
daemon listening on port 8000 on 127.0.0.1, use configuration options
like this:

::

    "hostname": "pump.yourdomain.example",
    "urlPort": 80,
    "address": "127.0.0.1",
    "port": 8000
