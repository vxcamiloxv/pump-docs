Configuration via environment variables
=======================================

.. NOTE:: This is the recommended configuration method for
          containerized pump.io daemons.

The ``pump`` daemon accepts configuration values via environment
variables. Each variable is prefixed with ``PUMPIO_`` and then has the
capitalized configuration key you want to set. For example, the
``port`` key can be set via the environment variable ``PUMPIO_PORT``.

To configure camelcased config values like ``urlPort``, replace the
camelcasing with an underscore (``_``). For example, ``urlPort`` would
become ``PUMPIO_URL_PORT``. Keys whose value is an object can be
specified using ``__`` (two underscores) to separate subkeys. For
example, the ``params`` key with the following object as its contents:

::

    { "host": "localhost" }

can be represented by exporting ``PUMPIO_PARAMS__HOST`` to the
environment with a value of ``localhost``. Exactly how to set
environment variables will depend on how you start the pump.io daemon.

Environment variables override JSON configuration files and are
overridden by CLI flags.

For a list of all available configuration values, see `the reference
list <reference.html>`_.
