Using Certbot and Let's Encrypt for HTTPS in pump.io
====================================================

After you have set up pump.io - but before you staring interacting
with other servers or clients - it's a good idea to set up TLS
(HTTPS), which prevents others from reading or tampering with users'
private data. It also helps keep the overall pump.io network secure
since other servers can deliver activities over TLS, giving their
users the same confidentiality and integrity guarantees. Changing
ports or from HTTP to HTTPS after interacting with other servers or
clients can cause problems. The `npm-based install instructions
<generic-npm.html>`_ talk about other changes that may also cause
problems.

`Let's Encrypt <https://letsencrypt.org/>`_ is an automated
Certificate Authority which will issue TLS certifiates for free. This
document will show you how to set up `Certbot
<https://certbot.eff.org/>`_, a popular Let's Encrypt client, to
acquire and automatically renew Let's Encrypt certificates.

.. NOTE:: These instructions do not work with Docker images.

First, install Certbot. Your GNU/Linux distribution may already have
it packaged for easy installation, or you may need to do that
yourself - either way, the `Certbot website
<https://certbot.eff.org/>`_ will tell you how to get it installed.

Once Certbot is installed run this command, inserting your actual email
address and domain names:

::

    $ certbot certonly --email user@host.root --webroot -w /usr/lib/node_modules/pump.io/public/ -d domain.tld,www.domain.tld

The email address provided will receive notifications when the
certificate is due to expire which can be helpful as a reminder.

Note that ``/usr/lib/node_modules/pump.io/`` is usually where npm
installs pump.io when you use an npm-based installation. If your setup
is different, provide the full path to your pump.io installation, plus
``/public/`` at the end.

Next, edit your pump.io configuration (for example,
``/etc/pump.io.json`` if you're using a `JSON file for configuration
<via-json-config-files.html>`_) and be sure that it contains these
settings (again replacing ``domain.tld`` with your production domain):

::

    "hostname":  "domain.tld",
    "key": "/etc/letsencrypt/live/domain.tld/privkey.pem",
    "cert": "/etc/letsencrypt/live/domain.tld/fullchain.pem",
    "port":  443,
    "bounce": true,
    "hsts": true,

Restart the pump.io software. Congratulations, you should now have TLS
working!

To automate renewals you can make a cron job or systemd timer that
runs ``certbot renew``. That should take care of renewing the
certificate but the email address provided during the initial setup
can be helpful as a fallback method in case something happens. The
pump.io software should also be restarted once the certificate is
renewed. This can be accomplished with `zero-downtime restarts
<../administration/zero-downtime-restarts.html>`_ and `Certbot pre/post
validation hooks
<https://certbot.eff.org/docs/using.html#pre-and-post-validation-hooks>`_.

For more on ``certbot renew``, consult the `Certbot documentation
<https://certbot.eff.org/docs/using.html#renewing-certificates>`_.
