Built-in CLI applications
=========================

You can use any pump.io `client application <clients.html>`_ you want
to interact with pump.io servers. However, the ``pump.io`` package
comes with some samples to get you started and you can find some more
in the repository.

pump-register-app
-----------------

First use this tool to create the credentials file

::

    $ ./bin/pump-register-app  -t <APPNAME>

``<APPNAME>`` will be the name of the client app that
``pump-register-app`` registers with the server.

This will create the file ``~/.pump.d/<SERVER>.json`` that contains your credentials.

::

    {
    "client_id":"XXXX",
    "client_secret":"YYYYY",
    "expires_at":0
    }

It will also add an entry into the server database where you will find
the clientID.

(Of course, if you use the memory Databank driver the data will be
lost between server runs, and you'll need to rerun the configuration.)

pump-register-user
------------------

Use this command to register a user:

::

    ./bin/pump-register-user  -u <USERNAME> -p <PASSWORD>

pump-authorize
--------------

After you register an app, you can authorize your user to use it.

::

    ./bin/pump-authorize -u <USERNAME>

When you do that it will ask you to open a website, login and verify the
value. You paste that back in and all is good.
