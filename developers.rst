For developers
==============

Eventually this section will be much more thorough, but in the
meantime you should check out `API.md
<https://github.com/pump-io/pump.io/blob/master/API.md>`_ for a
description of what the API looks like and what you can do with it.

Note that we are in the process of deprecating this API and moving to
`ActivityPub <https://www.w3.org/TR/activitypub/>`_ as well as OAuth
2.0. If you write things to the API described in ``API.md`` you
shouldn't have too much trouble moving to ActivityPub - ActivityPub is
actually derived from the original pump.io API! You can experiment
with this code as it lands by `running alpha builds
<https://github.com/pump-io/pump.io/wiki/Running-from-git-master>`_.
