:orphan:

Upgrading to pump.io 6.x from 5.x
==================================

These are the instructions for upgrading pump.io 5.x to 6.x. They will
work for any release in the 5.x series and can be used to upgrade to
any release in the 6.x series, including beta releases.

.. NOTE:: If you're on pump.io 4.x or below, it's recommended to
          upgrade to pump.io 5.x before attempting to upgrade to
          6.x. It's not strictly necessary, but it makes things easier
          if things go wrong since there's less changes that could be
          causing problems.

If at any point you run into trouble, contact the `community
<https://github.com/pump-io/pump.io/wiki/Community>`_ and they'll be
happy to sort you out.

For npm-based installs
----------------------

First, check what Node.js version you're using. Unless you've set up a
Node version manager, ``node --version`` should tell you this
information. If you're using Node 6, 8, or 9, you'll be able to
continue running pump.io without issues. If you're using Node 4, 5, or
7, see "Upgrading Node.js" below.

Next, check if your ``pump.io.json`` or environment configuration
contain ``nicknameBlacklist``, ``canUpload``, ``haveEmail``, or
``workers`` configuration keys. If they do, see "removing internal
configuration parameters" below.

Next, check if your ``pump.io.json`` or environment configuration do
`not` contain ``secret``, or set ``secret`` to the value ``my dog has
fleas`` (which is the value the sample configuration uses). If they
do, see "fixing ``secret``" below.

Lastly, note that pump.io 6.x removes ``SIGUSR2`` as the mechanism for
triggering zero-downtime restarts. If you previously made use of
``SIGUSR2``, see "migrating to the control socket" below for
information on its replacement and what you need to be aware of.

At this point, you should be ready to upgrade your installation.

To run the upgrade, invoke:

::

   $ sudo npm install -g pump.io@6

.. NOTE:: If you're trying to upgrade to the latest beta, specify
          ``pump.io@beta`` instead.

Finally, the CLI client tools that previously came with pump.io have
been extracted to a separate package. If you used anything besides
``pump``, you will need to install the standalone package.

You can do this with npm:

::

   $ [sudo] npm install -g pump.io-cli

Complete your upgrade by restarting your pump.io process, although
note that the CLI has gotten smarter about which options are booleans
and strings. If you were passing invalid configuration directly to the
CLI before, it may not be accepted anymore and you'll have to fix your
invocation.


For source-based installs
-------------------------

First, check what Node.js version you're using. Unless you've set up a
Node version manager, ``node --version`` should tell you this
information. If you're using Node 6, 8, or 9, you'll be able to
continue running pump.io without issues. If you're using Node 4, 5, or
7, see "Upgrading Node.js" below.

Next, check if your ``pump.io.json`` or environment configuration
contain ``nicknameBlacklist``, ``canUpload``, ``haveEmail``, or
``workers`` configuration keys. If they do, see "removing internal
configuration parameters" below.

Next, check if your ``pump.io.json`` or environment configuration do
`not` contain ``secret``, or set ``secret`` to the value ``my dog has
fleas`` (which is the value the sample configuration uses). If they
do, see "fixing ``secret``" below.

Lastly, note that pump.io 6.x removes ``SIGUSR2`` as the mechanism for
triggering zero-downtime restarts. If you previously made use of
``SIGUSR2``, see "migrating to the control socket" below for
information on its replacement and what you need to be aware of.

At this point, you should be ready to upgrade your installation.

To start the upgrade, fetch new changes, prefixing ``sudo`` as
necessary:

::

   $ cd /path/to/your/installation/directory
   $ git fetch

If you've modified templates, you need to save the changes you've
made. You can check to see whether you've made changes by running
``git status`` and ``git log`` - if you have untracked changes
reported by ``git status``, save them with ``git stash``; if you have
committed changes reported by ``git log``, make a note of the commit
ids.

Now that we're prepared, we'll perform the upgrade itself by checking
out the relevant tag. You'll have to check what the latest version in
the 6.x series is using ``git tag`` - in this case, we're assuming
that 6.0.0 is the latest available.

::

   $ git checkout v6.0.0
   $ npm install

At this point, if you previously ran ``git stash``, you should run
``git stash apply``. If you made a note of commits, you should tell
Git to apply those commits on the new checkout using ``git
cherry-pick``.

Next, you need to rebuild client-side template resources which aren't
checked into git:

::

   $ npm run build

If this command doesn't work, it's probably because utility templates
which you may be using in custom templates have been
reorganized. ``npm run build`` should have issued warnings about not
being able to find some includes. These have likely been moved from
``public/template/`` to ``public/template/lib/``; you will need to
adjust the paths in your custom templates to match.

We recommend that you run the linting process to ensure that your
changes are high-quality and consistent with surrounding code:

::

   $ npm run lint:jade

Finally, the CLI client tools that previously came with pump.io have
been extracted to a separate package. If you used anything besides
``bin/pump``, ``bin/pump-import-collection``,
``bin/pump-import-email``, or ``bin/pump-set-password``, you will need
to install the standalone package.

The recommended way to do this is with npm:

::

   $ [sudo] npm install -g pump.io-cli

You can also clone the `git repository
<https://github.com/pump-io/pump.io-cli>`_ and run ``npm install``,
but this is more error-prone and we really recommend that you install
via npm.

Complete your upgrade by restarting your pump.io process, although
note that the CLI has gotten smarter about which options are booleans
and strings. If you were passing invalid configuration directly to the
CLI before, it may not be accepted anymore and you'll have to fix your
invocation.

Upgrading Node.js
-----------------

As `previously announced
<http://pump.io/blog/2018/05/pump.io-5.1.1-docker-images-and-node-4>`_,
pump.io 6.0 drops support for Node 4, 5, and 7. These versions already
have no security support from Node.js upstream and were preventing
important maintenance work.

Most mainstream distributions ship supported versions of Node.js, so
if possible, you should upgrade your distribution. If this is not an
option, you can use `NodeSource
<https://github.com/nodesource/distributions>`_ to get a newer version
of Node, or you can switch to our `Docker images
<../installation/about-docker-images.html>`_ which use a modern version
of Node internally.

If you aren't sure how to move forward, contact the `community
<https://github.com/pump-io/pump.io/wiki/Community>`_ and they'll help
you sort through your options.

Removing internal configuration parameters
------------------------------------------

pump.io 6.0 checks configurations more strictly than earlier versions
to make sure that administrators aren't setting internal configuration
parameters by mistake. These internal parameters were always ignored
and overwritten by pump.io, so their usage never had any
effect. Therefore pump.io will now refuse to run with them set to
reduce confusion.

To resolve this issue, simply delete ``nicknameBlacklist``,
``canUpload``, ``haveEmail``, and ``workers`` from your
configuration. If you were expecting these options to do something, or
otherwise have questions, feel free to ask the `community
<https://github.com/pump-io/pump.io/wiki/Community>`_.

Fixing ``secret``
-----------------

pump.io 5.1 introduced log warnings when administrators didn't set
``secret`` in the configuration, or didn't change it after copying the
sample configuration file. Since these configurations represent a
potential security problem, pump.io 6.x changes these warnings into
fatal errors.

In order to fix these errors so that pump.io will start, make up a new
value for ``secret`` (either manually or by using a program that can
generate random strings, e.g. ``apg``). Then either add it as the
value of the ``secret`` key to your configuration if it wasn't there
before, or change ``my dog has fleas`` to your new secret if you have an existing ``secret`` value.

Migrating to the control socket
-------------------------------

TODO
