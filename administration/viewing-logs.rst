Viewing pump.io logs
====================

pump.io uses `Bunyan <https://github.com/trentm/node-bunyan>`_ for its
logs.

Bunyan comes with a command-line tool which can format your logs' JSON
into something much prettier.

Installing the Bunyan CLI
-------------------------

The Bunyan CLI can be installed with ``npm``:

::

   $ sudo npm install -g bunyan

Viewing your logs
-----------------

To view your logs, just invoke the Bunyan CLI with the log filename
specified in your ``pump.io.json``, or pipe from stdin.

For example, to just pretty-print pump.io's output:

::

   $ pump 2>&1 | bunyan

.. NOTE:: You need ``2>&1`` because pump.io prints logs to stderr.

Or, if your logs are stored at ``/var/log/pump.io/pump.io.log.json``:

::

   $ bunyan /var/log/pump.io/pump.io.log.json

Bunyan logfiles can get quite large and ``bunyan`` can take a while to
format large files, so it may be smart to only view the latest logs:

::

   $ tail /var/log/pump.io/pump.io.log.json | bunyan

Filtering logs
--------------

Bunyan logs are structured. You can filter them to only show (for
example) certain types of messages using the ``-l`` flag.

Valid loglevels are ``fatal``, ``error``, ``warn``, ``info``, and
``debug``. See the `Bunyan documentation
<https://github.com/trentm/node-bunyan#levels>`_ for details on what
these levels mean, and note that pump.io does not use the ``trace``
loglevel at all.

For example, if we wanted to show only ``fatal`` and ``error`` messages:

::

   $ bunyan -l error

.. NOTE:: if you're trying to view ``debug`` messages but aren't
   seeing any, pump.io is probably not writing ``debug`` log
   messages. Try adjusting the ``logLevel`` config option.

The Bunyan CLI has more options for filtering and output: you can see
them by invoking ``bunyan --help``.
